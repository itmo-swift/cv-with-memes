//
//  ViewController.swift
//  CVWithMemes
//
//  Created by Daniil Lushnikov on 29.09.2021.
//

import UIKit

class MemesController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    @IBAction func switcherAction(_ sender: Any) {
        print(#function)
    }
    @IBAction func sliderAction(_ sender: Any) {
        print(#function)
    }
    @IBAction func stepperAction(_ sender: Any) {
        print(#function)
    }
    @IBAction func mem1StepperAction(_ sender: Any) {
        print("Mem 1: \(#function)")
    }
    @IBAction func mem2StepperAction(_ sender: Any) {
        print("Mem 2: \(#function)")
    }
    @IBAction func mem3StepperAction(_ sender: Any) {
        print("Mem 3: \(#function)")
    }
    
}
