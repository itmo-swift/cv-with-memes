//
//  ViewController.swift
//  CVWithMemes
//
//  Created by Daniil Lushnikov on 28.09.2021.
//

import UIKit

class CvController: UIViewController {
    @IBOutlet weak var aboutMeTitle: UILabel!
    @IBOutlet weak var aboutMeView: UIView!
    @IBOutlet weak var educationView: UIView!
    @IBOutlet weak var languageView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareView()
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        print("After view became visible for user and before animation is set up. Function: \(#function)")
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        print("Dimensions of the view changed and the position of the child views needs to be adjusted. Function: \(#function)")
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        print("After the position of the child views has been adjusted. Function: \(#function)")
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("After view became visible for user on a screen. Function: \(#function)")
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        print("Before VC deleting from view hierarchy. Function: \(#function)")
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("After VC deleting from view hierarchy. Function: \(#function)")
    }

    deinit {
        print("Destructor. Function: \(#function)")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        print("Memory clean before deleting app. Function: \(#function)")
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        print("Screen orientation changed and you need to calculate new parameters for the view and child elements. Function: \(#function)")
    }
    
    private func prepareView() {
        aboutMeView.dropShadow()
        educationView.dropShadow()
        languageView.dropShadow()
        aboutMeView.layoutMargins = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        educationView.layoutMargins = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        languageView.layoutMargins = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
    }
}

extension UIView {
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = .zero
        layer.shadowRadius = 5
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}
